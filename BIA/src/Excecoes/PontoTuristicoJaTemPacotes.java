package Excecoes;

public class PontoTuristicoJaTemPacotes extends Exception{

    @Override
    public String getMessage(){
        return "O ponto turístico faz parte de pacotes, deseja continuar?";
    }
}
