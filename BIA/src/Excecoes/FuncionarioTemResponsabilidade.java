/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Excecoes;

/**
 *
 * @author Aluno
 */
public class FuncionarioTemResponsabilidade extends Exception{
    @Override
    public String getMessage(){
        return "Funcionario é responsável por uma ou mais cobranças, deseja continuar?";
    }
}
