/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Excecoes;

/**
 *
 * @author Cliente
 */
public class VeiculoJaTemViagens extends Exception {
    @Override
    public String getMessage(){
        return "O veículo já apresenta viagens feitas, deseja continuar?";
    }
}
