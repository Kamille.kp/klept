package Excecoes;

public class ClienteJaTemViagens extends Exception{

    @Override
    public String getMessage(){
        return "O cliente já apresenta viagens feitas, deseja continuar?";
    }
}
