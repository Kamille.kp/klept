package Usuario;

import Model.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DonoDaEmpresa extends Operador{
    private static boolean online = false;
    
    public static void setOnline(boolean online) {
        DonoDaEmpresa.online = online;
    }
    
    public static void setDono(){
        Conexao c = new Conexao();
        Statement s;
        String SQLSelect = "select f.cpf,f.login,f.senha from funcionario f where f.login like 'adm%'";
        
        try{
            s = c.getConexao().createStatement();
             
            ResultSet rs = s.executeQuery(SQLSelect);
            
            if(rs.next()){
                DonoDaEmpresa.setLogin(rs.getString("login"));
                DonoDaEmpresa.setSenha(rs.getString("senha"));
                DonoDaEmpresa.setInfo(Funcionario.getOne(rs.getInt("cpf")));
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
    }
}