package Usuario;

import Excecoes.UsuarioInvalido;
import Model.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
/**
 *
 * @author Cliente
 */
public class Operador {
    private static String login;
    private static String senha;
    private static Funcionario info;

    public static String getSenha() {
        return senha;
    }

    public static void setSenha(String senha) {
        Operador.senha = senha;
    }

    public static Funcionario getInfo() {
        return info;
    }

    public static void setInfo(Funcionario info) {
        Operador.info = info;
    }
    
    public static String getLogin(){
        return login;
    }
    
    public static void setLogin(String login){
        Operador.login = login;
    }
    
    public static void testaLogin() throws UsuarioInvalido{
        Conexao c = new Conexao();
        PreparedStatement ps;
        String pegaLoginSenha = "select cpf,login,senha from funcionario where login = ? and senha = ?";
        
        try{
            ps = c.getConexao().prepareStatement(pegaLoginSenha);
            ps.setString(1, login);
            ps.setString(2, senha);
            
            ResultSet rs = ps.executeQuery();
            
            if(rs.next()){
                Operador.setInfo(Funcionario.getOne(rs.getLong("cpf"))); //Carrega as informações do operador
                if(DonoDaEmpresa.getLogin().equals(login)){//Testa se o operador é o dono da empresa
                    DonoDaEmpresa.setOnline(true);
                }
            }
            else{
                throw new UsuarioInvalido();
            }
            
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }
}
