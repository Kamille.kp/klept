package Model;

import Excecoes.VeiculoJaTemViagens;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import lombok.Data;

public @Data class Veiculo {
    private Date datacadastro;
    private String anoFabricacao;
    private long renavam;
    private String numeroChassi;
    private String placa;
    private int quilometragem;
    private String descricao;
    private TipoVeiculo tipo;
    private boolean status;
    
    public static ArrayList<Veiculo> getAll(){
        ArrayList<Veiculo> lista = new ArrayList<Veiculo>();
        Veiculo v;
        
        Conexao c = new Conexao();
        Statement s;
        String SQLSelect = "select * from veiculo";
        
        try{
            s = c.getConexao().createStatement();
            
            ResultSet rs = s.executeQuery(SQLSelect);
            
            while(rs.next()){
                v = new Veiculo();
                
                v.setRenavam(rs.getLong("renavam"));
                v.selectAtributosVeiculo(rs);
                lista.add(v);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        
        return lista;
    }
    
    public static ArrayList<Veiculo> getAllAtivos(){
        ArrayList<Veiculo> lista = new ArrayList<Veiculo>();
        Veiculo v;
        
        Conexao c = new Conexao();
        Statement s;
        String SQLSelect = "select * from veiculo where status = 1";
        
        try{
            s = c.getConexao().createStatement();
            
            ResultSet rs = s.executeQuery(SQLSelect);
            
            while(rs.next()){
                v = new Veiculo();
                
                v.setRenavam(rs.getLong("renavam"));
                v.selectAtributosVeiculo(rs);
                lista.add(v);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        
        return lista;
    }

    public static Veiculo getOne(long id) {
        Veiculo v = new Veiculo();
        
        Conexao c = new Conexao();
        PreparedStatement ps;
        String SQLSelect = "select * from veiculo where renavam = ?";
        
        try{
            ps = c.getConexao().prepareStatement(SQLSelect);
            ps.setLong(1, id);
            
            ResultSet rs = ps.executeQuery();
            
            if(rs.next()){
                v.setRenavam(id);
                v.selectAtributosVeiculo(rs);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
        
        return v;
    }
    
    public void update(){
        Conexao c = new Conexao();
        PreparedStatement ps;
        String SQLUpdate = "update veiculo set"
                        + "ano_fabricacao = ?,"
                        + "numero_chassi = ?,"
                        + "descricao = ?,"
                        + "status = ?,"
                        + "where placa = ?";
        
        try{
            ps = c.getConexao().prepareStatement(SQLUpdate);
            ps.setString(5, this.placa);
            
            ps.setString(1, this.anoFabricacao);
            ps.setString(2, this.numeroChassi);
            ps.setString(3, this.descricao);
            ps.setBoolean(4, this.status);
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }
    
    public void delete() throws VeiculoJaTemViagens{
        Conexao c = new Conexao();
        PreparedStatement ps;
        String SQLDelete = "delete from veiculo where placa = ?";
        
        if(Viagem.verificarVeiculoConexoes(this)){
            throw new VeiculoJaTemViagens();
        }
        
        try{
            ps = c.getConexao().prepareStatement(SQLDelete);
            ps.setString(1, this.placa);
            
            ps.executeUpdate();
        } 
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void tornaInativo(){
        this.status = false;
        this.update();
    }

    private void selectAtributosVeiculo(ResultSet rs) {
        try{
            this.datacadastro = rs.getDate("data_cadastro_veiculo");
            this.anoFabricacao = rs.getString("ano_fabricacao");
            this.numeroChassi = rs.getString("numero_chassi");
            this.placa = rs.getString("placa");
            this.descricao = rs.getString("descricao");
           
        }
        catch(SQLException e){
            System.out.println("Erro ao carregar veiculo: " + e.getMessage());
        }
    }
}
