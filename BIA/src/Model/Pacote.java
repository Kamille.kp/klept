package Model;

import Excecoes.PacoteJaTemViagens;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import lombok.Data;

public @Data class Pacote {
    private int id;
    private String descricao;
    private double preco;
    private boolean status = true;
    private ArrayList<PontoTuristico> pontoTuristicos;
    
    public static boolean verificarConexoes(PontoTuristico pt) {
        boolean esta = false;
        for(Pacote p : Pacote.getAll()){
            if(p.getPontoTuristicos().contains(pt)){
                esta = true;
            }
        } 
        return esta;
    }
    
    public static ArrayList<Pacote> getAll() {
        ArrayList<Pacote> lista = new ArrayList<>();
        Pacote p;
        
        Conexao c = new Conexao();
        Statement s;
        String SQLSelect = "select * from pacote";
        
        try{
            s = c.getConexao().createStatement();
            
            ResultSet rs = s.executeQuery(SQLSelect);
            
            while(rs.next()){
                p = new Pacote();
                p.setId(rs.getInt("id_pacote"));
                p.selectAtributos(rs);
                
                lista.add(p);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
        
        return lista;
    }

    public static ArrayList<Pacote> getAllAtivos() {
        ArrayList<Pacote> lista = new ArrayList<>();
        Pacote p;
        
        Conexao c = new Conexao();
        Statement s;
        String SQLSelect = "select * from pacote where status = 1";
        
        try{
            s = c.getConexao().createStatement();
            
            ResultSet rs = s.executeQuery(SQLSelect);
            
            while(rs.next()){
                p = new Pacote();
                p.setId(rs.getInt("id_pacote"));
                p.selectAtributos(rs);
                
                lista.add(p);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
        
        return lista;
    }

    public void load() {
        PreparedStatement ps;
        Conexao c = new Conexao();
        String SQLLoad = "select * from pacote where id_pacote = ?";
        
        try{
            ps = c.getConexao().prepareStatement(SQLLoad);
            
            ps.setInt(1,this.id);
            
            ResultSet rs = ps.executeQuery();
            
            if(rs.next()){
                this.selectAtributos(rs);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }
    
    public void update(){
        Conexao c = new Conexao();
        PreparedStatement ps;
        String SQLUpdate = "update pacote set "
                                    + "descricao = ?,"
                                    + "preco = ?,"
                                    + "status = ? "
                                    + "where id = ?";
        
        try{
            ps = c.getConexao().prepareStatement(SQLUpdate);
            
            ps.setInt(4,this.id);
            
            ps.setString(1, this.descricao);
            ps.setDouble(2, this.preco);
            ps.setBoolean(3, this.status);
            
            ps.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }
    
    public void delete() throws PacoteJaTemViagens{
        Conexao c = new Conexao();
        PreparedStatement ps;
        String SQLDelete = "delete from pacote where id_pacote = ?";
        
        try{
            if(Viagem.verificarPacoteConexoes(this)){
                throw new PacoteJaTemViagens();
            }
            else{
                ps = c.getConexao().prepareStatement(SQLDelete);
                ps.setInt(1, this.id);
        
                ps.executeUpdate();
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }
    
    public void tornaInativo(){
        this.status = false;
        this.update();
    }

    private void selectAtributos(ResultSet rs){
        try{
            this.descricao = rs.getString("descricao");
            this.preco = rs.getDouble("preco_pacote");
            this.status = rs.getBoolean("status");
            
            this.loadPontosTuristicos();
        }
        catch(SQLException e){
            System.out.println("Erro ao carregar o pacote: " + e.getMessage());
        }
    }

    private void loadPontosTuristicos() {
        Conexao c = new Conexao();
        PreparedStatement ps;
        String pegaPontosTuristicos = "select id_ponto_turistico "
                                + "from ponto_turistico_pacote "
                                + "where id_pacote = ?";
        
        PontoTuristico pt;
        this.pontoTuristicos = new ArrayList<>();
        
        try{
            ps = c.getConexao().prepareStatement(pegaPontosTuristicos);
            ps.setInt(1,this.id);
            
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){
                pt = new PontoTuristico();
                pt.setId(rs.getInt("id_ponto_turistico"));
                pt.load();
                
                this.pontoTuristicos.add(pt);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }

    @Override
    public String toString(){
        String pontosNomes = "";
        
        for(PontoTuristico pt : this.pontoTuristicos){
            pontosNomes += ("\n- " + pt.getNome());
        }
        
        return "ID: " + this.id + "\n" +
               "Preço: " +this.preco + "\n" +
               "Pontos Turisticos:" + pontosNomes;
    }
}