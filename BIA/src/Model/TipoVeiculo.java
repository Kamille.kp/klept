/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import lombok.Data;

/**
 *
 * @author Aluno
 */
public @Data class TipoVeiculo {
    private int id;
    private String nome;
    private String combustivel;
    private int quantLugares;
    private boolean status;
    
    public void insert(){
        Conexao c = new Conexao();
        PreparedStatement ps;
        String SQLInsert =  "insert into tipo_veiculo values(seq_tipoveiculo.nextval,?,?,?,1)";
        
        try{
            ps = c.getConexao().prepareStatement(SQLInsert);
            ps.setString(1, this.combustivel);
            ps.setString(2, this.nome);
            ps.setInt(3, this.quantLugares);
            
            ps.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }
    
    public static ArrayList<TipoVeiculo> getAllAtivos(){
        ArrayList<TipoVeiculo> lista = new ArrayList<>();
        TipoVeiculo tv;
        
        Conexao c = new Conexao();
        Statement s;
        String SQLSelect = "select * from tipo_veiculo where status = 1";
        
        try{
            s = c.getConexao().createStatement();
            
            ResultSet rs = s.executeQuery(SQLSelect);
            
            while(rs.next()){
                tv = new TipoVeiculo();
                tv.setId(rs.getInt("id_tipo_veiculo"));
                tv.setCombustivel(rs.getString("combustivel"));
                tv.setNome(rs.getString("nome"));
                tv.setQuantLugares(rs.getInt("quantidade_lugares"));
                tv.setStatus(rs.getBoolean("status"));
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
        
        return lista;
    }
    
    public static ArrayList<TipoVeiculo> getAll(){
        ArrayList<TipoVeiculo> lista = new ArrayList<>();
        TipoVeiculo tv;
        
        Conexao c = new Conexao();
        Statement s;
        String SQLSelect = "select * from tipo_veiculo";
        
        try{
            s = c.getConexao().createStatement();
            
            ResultSet rs = s.executeQuery(SQLSelect);
            
            while(rs.next()){
                tv = new TipoVeiculo();
                tv.setId(rs.getInt("id_tipo_veiculo"));
                tv.setCombustivel(rs.getString("combustivel"));
                tv.setNome(rs.getString("nome"));
                tv.setQuantLugares(rs.getInt("quantidade_lugares"));
                tv.setStatus(rs.getBoolean("status"));
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
        
        return lista;
    }
    
    public static TipoVeiculo getOne(int id){
        TipoVeiculo tv = new TipoVeiculo();
        
        Conexao c = new Conexao();
        PreparedStatement ps;
        String SQLSelect = "select * from tipo_veiculo where id_tipo_veiculo = ?";
        
        try{
            ps = c.getConexao().prepareStatement(SQLSelect);
            ps.setInt(1, id);
            
            ResultSet rs = ps.executeQuery();
            
            if(rs.next()){
                tv.selectAtributos(rs);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
        
        return tv;
    }
    
    public void update(){
        Conexao c = new Conexao();
        PreparedStatement ps;
        String SQLUpdate = "Update tipo_veiculo set"
                        + "nome = ?,"
                        + "combustivel = ?,"
                        + "quantidade_lugares = ?,"
                        + "status = ? "
                        + "where id_tipo_veiculo = ?";
        
        try{
            ps = c.getConexao().prepareStatement(SQLUpdate);
            
            ps.setInt(5, this.id);
            
            ps.setString(1, this.nome);
            ps.setString(2, this.combustivel);
            ps.setInt(3, this.quantLugares);
            ps.setBoolean(4, this.status);
            
            ps.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    private void selectAtributos(ResultSet rs){
        try{
            this.id = rs.getInt("id_tipo_veiculo");
            this.combustivel = rs.getString("combustivel");
            this.nome = rs.getString("nome");
            this.quantLugares = rs.getInt("quantidade_lugares");
            this.status = rs.getBoolean("status") ;
        }
        catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    public void tornaInativo(){
        this.status = false;
        
        for(Veiculo v : Veiculo.getAllAtivos()){
            if(v.getTipo().equals(this)){
                v.tornaInativo();
            }
        }
        
        this.update();
    }
    
    @Override
    public String toString(){
        return this.nome;
    }
}