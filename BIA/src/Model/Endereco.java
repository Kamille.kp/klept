package Model;
        
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;

public @Data class Endereco {
    private int id;
    private int cep;
    private String estado;
    private int numero;
    private String cidade;
    private String bairro;
    private String logradouro;
    private String complemento;
    
    public int insert(){
        Conexao c = new Conexao();
        int genId = 0;
        String genKey[] = {"id_endereco"};
        PreparedStatement ps;
        String SQLInsert = "insert into endereco values(?,?,?,?,?,?,?,seq_end.nextval,1)";
        
        try{
            ps = c.getConexao().prepareStatement(SQLInsert,genKey);
            
            this.setAtributos(ps);
            
            ps.executeUpdate();
            
            ResultSet rs = ps.getGeneratedKeys();
            
            if(rs.next()){
                genId = rs.getInt(1);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
        
        return genId;
    }
    
    public void update(){
        Conexao c = new Conexao();
        PreparedStatement ps;
        String SQLUpdate = "update endereco set"
                                    + "logradouro = ?, numero = ?,"
                                    + "complemento = ?, bairro = ?,"
                                    + "cidade = ?, estado = ?,"
                                    + "cep = ?"
                                    + "where id = ?";
        
        try{
            ps = c.getConexao().prepareStatement(SQLUpdate);
            
            ps.setInt(8,this.id);
            
            this.setAtributos(ps);
            
            ps.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }
    
    public void delete(){
        Conexao c = new Conexao();
        PreparedStatement ps;
        String SQLDelete = "delete from endereco where cpf = ?";
        
        try{
            ps = c.getConexao().prepareStatement(SQLDelete);
            
            ps.setInt(1, this.id);
            
            ps.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }

    public void load() {
        PreparedStatement ps;
        Conexao c = new Conexao();
        String SQLLoad = "select * from endereco where id_endereco = ?";
        
        try{
            ps = c.getConexao().prepareStatement(SQLLoad);
            
            ps.setInt(1,this.id);
            
            ResultSet rs = ps.executeQuery();
            
            if(rs.next()){
                this.logradouro = rs.getString("logradouro");
                this.numero = rs.getInt("numero");
                this.complemento = rs.getString("complemento");
                this.bairro = rs.getString("bairro");
                this.cidade = rs.getString("cidade");
                this.estado = rs.getString("estado");
                this.cep = rs.getInt("cep");
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }
    
    
    public static List<Endereco> getAll() {
        PreparedStatement ps;
        Conexao c = new Conexao();
        String SQLLoad = "select * from endereco";
        List<Endereco> ends = new ArrayList<>();
        try{
            ps = c.getConexao().prepareStatement(SQLLoad);
            
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){
                Endereco e = new Endereco();
                e.logradouro = rs.getString("logradouro");
                e.numero = rs.getInt("numero");
                e.complemento = rs.getString("complemento");
                e.bairro = rs.getString("bairro");
                e.cidade = rs.getString("cidade");
                e.estado = rs.getString("estado");
                e.cep = rs.getInt("cep");
                ends.add(e);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
        return ends;
    }
    private void setAtributos(PreparedStatement ps){
        try{
            ps.setString(1,this.logradouro);
            ps.setInt(2, this.numero);
            ps.setString(3, this.complemento);
            ps.setString(4, this.bairro);
            ps.setString(5, this.cidade);
            ps.setString(6, this.estado);
            ps.setInt(7, this.cep);
        }
        catch(SQLException e){
            System.out.println("Erro ao setar os atributos de endereço: " + e.getMessage());
        }
    }
    
    @Override
    public String toString(){
        return this.logradouro + ", " +
               this.numero + ", " +
               this.bairro + ", " +
               this.cidade + " - " +
               this.estado + "\n"+
               "CEP: " + this.cep;
    }
}
