package Model;

import Excecoes.PontoTuristicoJaTemPacotes;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import lombok.Data;

public @Data class PontoTuristico {
    private int id;
    private Endereco endereco;
    private String nome;
    private String descricao;
    private boolean status;
    
    public void insert(){
        Conexao c = new Conexao();
        PreparedStatement ps;
        String SQLInsert = "insert into ponto_turistico values(seq_pontotur.nextval,?,?,?,1)";
        
        try{
            ps = c.getConexao().prepareStatement(SQLInsert);
            
            ps.setInt(1, this.endereco.getId());
            ps.setString(2, this.getNome());
            ps.setString(3, this.getDescricao());
            
            ps.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }
    
    public static ArrayList<PontoTuristico> getAll(){
        ArrayList<PontoTuristico> lista = new ArrayList<>();
        PontoTuristico pt;
        
        Conexao c = new Conexao();
        Statement s;
        String SQLSelect = "select * from ponto_turistico";
        
        try{
            s = c.getConexao().createStatement();
            
            ResultSet rs = s.executeQuery(SQLSelect);
            
            while(rs.next()){
                pt = new PontoTuristico();
                pt.setId(rs.getInt("id_ponto_turistico"));
                pt.selectAtributos(rs);
                
                lista.add(pt);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
        
        return lista;
    }

    public void load() {
        Conexao c = new Conexao();
        PreparedStatement ps;
        String SQLSelect = "select * from ponto_turistico where id_ponto_turistico = ?";
        
        try{
            ps = c.getConexao().prepareStatement(SQLSelect);
            ps.setInt(1, this.id);
            
            ResultSet rs = ps.executeQuery();
            
            if(rs.next()){
                this.selectAtributos(rs);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }
    
    public void update(){
        Conexao c = new Conexao();
        PreparedStatement ps;
        String SQLUpdate = "update ponto_turistico set "
                                                    + "nome = ?,"
                                                    + "descricao = ?"
                                                    + "status = ?"
                                                    + "where id = ?";
        
        try{
            ps = c.getConexao().prepareStatement(SQLUpdate);
            
            ps.setInt(4, this.id);
            
            ps.setString(1, this.nome);
            ps.setString(2, this.descricao);
            ps.setBoolean(3, this.status);
            
            ps.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }
    
    public void delete() throws PontoTuristicoJaTemPacotes{
        Conexao c = new Conexao();
        PreparedStatement ps;
        String SQLDelete = "delete from ponto_turistico where id = ?";
        
        try{
            if(Pacote.verificarConexoes(this)){
                ps = c.getConexao().prepareStatement(SQLDelete);
                ps.setInt(1, this.id);
                
                ps.executeUpdate();
            }
            else{
                throw new PontoTuristicoJaTemPacotes();
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }

    public void tornaInativo() {
        this.status = false;
        for(Pacote p : Pacote.getAll()){
            if(p.getPontoTuristicos().contains(this)){
                p.tornaInativo();
            }
        }
        this.update();
    }

    private void selectAtributos(ResultSet rs) {
        Endereco en = new Endereco();
        try{
            this.descricao = rs.getString("descricao");
            this.nome = rs.getString("nome");
            this.status = rs.getBoolean("status");
            en.setId(rs.getInt("endereco"));
            en.load();
            this.setEndereco(en);
        }
        catch(SQLException e){
            System.out.println("Erro ao carregar os pontos turisticos: " + e.getMessage());
        }
    }
    
    @Override
    public String toString(){
        return "Nome: " + this.nome + "\n" +
               "Descricao: " + this.descricao + "\n" +
               "Endereço:\n" + this.endereco;
    }
}