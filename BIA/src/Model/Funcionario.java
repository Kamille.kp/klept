package Model;

import Excecoes.ClienteJaTemViagens;
import Excecoes.SenhasDiferentes;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import lombok.Data;

public @Data class Funcionario extends Pessoa{
    private double salario;
    private String senha;
    private String login;
    
    @Override
    public void insert(){
        Conexao c = new Conexao();
        PreparedStatement ps;
        String SQLInsert = "Insert into funcionario values(?,?,?,?)";
        Pessoa p = this;
        p.insert();
        
        try{
            ps = c.getConexao().prepareStatement(SQLInsert);
            
            ps.setLong(1, this.getCpf());
            ps.setDouble(2, this.salario);
            ps.setString(3, this.senha);
            ps.setString(4, this.login);
            
            ps.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }
    
    @Override
    public void delete() throws ClienteJaTemViagens{
        Conexao c = new Conexao();
        PreparedStatement ps;
        Pessoa p = this;
        String SQLDelete = "delete from funcionario where cpf = ?";
        
        try{
            ps = c.getConexao().prepareStatement(SQLDelete);
            ps.setLong(1, this.getCpf());
            
            ps.execute();
            
            p.delete();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }
    
    public static ArrayList<Funcionario> getAll(){
        Conexao c = new Conexao();
        ArrayList<Funcionario> lista = new ArrayList<>();
        Statement s;
        String SQLSelect = "select * from pessoa p inner join funcionario f on f.cpf = p.id";
        Funcionario f;
        
        try{
            s = c.getConexao().createStatement();
            
            ResultSet rs = s.executeQuery(SQLSelect);
            
            while(rs.next()){
                f = new Funcionario();
                f.setCpf(rs.getLong("cpf"));
                f.selectAtributos(rs);
                f.selectMaisAtributos(rs);
                lista.add(f);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
        
        return lista;
    }
    
    public static Funcionario getOne(long id){
        Conexao c = new Conexao();
        PreparedStatement ps;
        String SQLSelect = "select * from pessoa p inner join funcionario f on f.cpf = p.id where p.id = ?";
        Funcionario f = new Funcionario();
        
        try{
            ps = c.getConexao().prepareStatement(SQLSelect);
            
            ps.setLong(1,id);
            
            ResultSet rs = ps.executeQuery();
            
            if(rs.next()){
                f.setCpf(id);
                f.selectAtributos(rs);
                f.selectMaisAtributos(rs);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
        
        return f;
    }
    
    public void setSenhas(String senha, String repSenha) throws SenhasDiferentes{
        if(senha.equals(repSenha)){
            this.senha = senha;
        }
        else{
            throw new SenhasDiferentes();
        }
    }

    protected void selectMaisAtributos(ResultSet rs) {
        try{
            this.salario = rs.getDouble("salario");
            this.senha = rs.getString("senha");
            this.login = rs.getString("login");
        }
        catch(SQLException e){
            System.out.println("Erro ao carregar funcionario: " + e.getMessage());   
        }
    }

    @Override
    public String toString(){
        return "Nome: " + this.getNome() + "\n" +
               "Salário: " + this.salario + "\n" +
               "Login: " + this.login + "\n" +
               "Senha: " + this.senha + "\n" +
               "Nascimento: " + this.getDataNascimento() + "\n" +
               "CPF: " + this.getCpf() + "\n" +
               "Telefone: " + this.getTelefone() + "\n" +
               "Endereço:\n " + this.getEndereco();
    }
}
