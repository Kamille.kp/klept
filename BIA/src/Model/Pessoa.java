package Model;

import Excecoes.DataInvalida;
import Excecoes.ClienteJaTemViagens;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.sql.Date;
import lombok.Data;

public @Data class Pessoa {
    private String nome;
    private long cpf;
    private Date dataNascimento;
    private int telefone;
    private Endereco endereco = new Endereco();
    private Date dataCadastro;
    private boolean status = true;
    
    public String getStatus(){
        return (this.status ? "Ativo" : "Inativo");
    }
    
    public void setDataNascimento(String data) throws DataInvalida{
        String date[] = data.split("/");
        if(date.length == 3){
            if(date[0].length() == 2 && date[1].length() == 2 && date[2].length() == 4){
                data = date[2] + "-" + date[1] + "-" + date[0];
                this.dataNascimento = Date.valueOf(data);
            }
            else{
                throw new DataInvalida();
            }
        }
        else{
            throw new DataInvalida();
        }
    }
    
    public String getDataNascimento(){
        String data;
        String[] date = String.valueOf(this.dataCadastro).split("-");
        
        data = date[2] + "/" + date[1] + "/" + date[0];
        
        return data;
    }
    
    public String getDataCadastro(){
        String data;
        String[] date = String.valueOf(this.dataCadastro).split("-");
        
        data = date[2] + "/" + date[1] + "/" + date[0];
        
        return data;
    }
    
    public void insert(){
        Conexao c = new Conexao();
        PreparedStatement ps;
        String SQLInsert = "insert into pessoa"
                                    + " values(?,?,?,?,?,?,1)";
        
        try{
            ps = c.getConexao().prepareStatement(SQLInsert);
            
            ps.setLong(1,this.cpf);
            ps.setString(2, this.nome);
            ps.setDate(3, (java.sql.Date) this.dataNascimento);
            ps.setInt(4, this.telefone);
            ps.setDate(5, (java.sql.Date) this.dataCadastro);
            ps.setInt(6, this.endereco.insert());
            
            ps.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }
    
    public void update(){
        Conexao c = new Conexao();
        PreparedStatement ps;
        String SQLUpdate = "update pessoa set "
                                    + "nome = ?,"
                                    + "data_nascimento = ?,"
                                    + "telefone = ?,"
                                    + "status = ? "
                                    + "where id = ?";
        
        try{
            ps = c.getConexao().prepareStatement(SQLUpdate);
            
            ps.setLong(5,this.cpf);
            
            ps.setString(1, this.nome);
            ps.setDate(2, (java.sql.Date) this.dataNascimento);
            ps.setInt(3, this.telefone);
            ps.setBoolean(4, this.status);
            
            ps.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }
    
    public void delete() throws ClienteJaTemViagens{
        Conexao c = new Conexao();
        PreparedStatement ps;
        String SQLDelete = "delete from pessoa where id = ?";
        
        try{
            if(Viagem.verificarClienteConexoes(this)){
                throw new ClienteJaTemViagens();
            }
            
            ps = c.getConexao().prepareStatement(SQLDelete);
            
            ps.setLong(1,this.cpf);
            
            ps.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
    //            c.desconecta();
        }
    }

    public void load(){
        PreparedStatement ps;
        Conexao c = new Conexao();
        String SQLLoad = "select * from pessoa where id = ?";
        
        try{
            ps = c.getConexao().prepareStatement(SQLLoad);
            
            ps.setLong(1,this.cpf);
            
            ResultSet rs = ps.executeQuery();
            
            if(rs.next()){
                this.selectAtributos(rs);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }
    
    public static ArrayList<Pessoa> getClientes(){
        ArrayList<Pessoa> lista = new ArrayList<Pessoa>();
        Conexao c = new Conexao();
        Statement ps;
        String SQLSelect = "select * from pessoa p left outer join funcionario f on p.id = f.cpf where f.cpf is null";
        Pessoa p;
        
        try{
            ps = c.getConexao().createStatement();
            
            ResultSet rs = ps.executeQuery(SQLSelect);
            
            while(rs.next()){
                p = new Pessoa();
                p.setCpf(rs.getLong("id"));
                p.selectAtributos(rs);
                lista.add(p);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
        
        return lista;
    }
    
    public static ArrayList<Pessoa> getClientesAtivos(){
        ArrayList<Pessoa> lista = new ArrayList<Pessoa>();
        Conexao c = new Conexao();
        Statement ps;
        String SQLSelect = "select p.* from pessoa p left outer join funcionario f on p.id = f.cpf where f.cpf is null and status = 1";
        Pessoa p;
        
        try{
            ps = c.getConexao().createStatement();
            
            ResultSet rs = ps.executeQuery(SQLSelect);
            
            while(rs.next()){
                p = new Pessoa();
                p.setCpf(rs.getLong("id"));
                p.selectAtributos(rs);
                lista.add(p);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
        System.out.println("AAAAAAAAAAAAAAAAA");
        return lista;
    }
    
    public void tornaInativo(){
        this.status = false;
        this.update();
    }

    protected void selectAtributos(ResultSet rs){
        try{
            this.dataCadastro = rs.getDate("data_cadastro");
            this.nome = rs.getString("nome");
            this.dataNascimento = rs.getDate("data_nascimento");
            this.telefone = rs.getInt("telefone");
            this.endereco.setId(rs.getInt("endereco"));
            this.status = rs.getBoolean("status");
            this.endereco.load();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    @Override
    public String toString(){
        return "Nome: " + this.getNome() + "\n" +
               "Nascimento: " + this.getDataNascimento() + "\n" +
               "CPF: " + this.getCpf() + "\n" +
               "Telefone: " + this.getTelefone() + "\n" +
               "Endereço:\n " + this.getEndereco();
    }
}