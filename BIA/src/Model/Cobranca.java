package Model;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import lombok.Data;

public @Data class Cobranca{
    private int id;
    private String descricao;
    private String tipoPagamento;
    private Date validade;
    private boolean status = false;
    
    public String getStatus(){
        return (this.status) ? "Pago" : "Não pago";
    }
    
    public void insert(){
        Conexao c = new Conexao();
        PreparedStatement ps;
        String SQLInsert = "insert into tipo_pagamento values(seq_cobranca,?,?)";
        
        try{
          ps = c.getConexao().prepareStatement(SQLInsert);
          ps.setString(1, this.descricao);
          ps.setString(2, this.tipoPagamento);
          
          ps.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }
    
    public static Cobranca getOne(int id){
        return new Cobranca();
    }
    
    public void getAll(){
        
    }
    
    public void update(){
        
    }
    
    public void delete(){
        
    }
    
    public void tornaPago(){
        this.status = true;
        this.update();
    }
}
