package Model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import lombok.Data;

public @Data class Motorista extends Funcionario{
    private long numeroCNH;
    private Date validadeCNH;
    private int diasAviso;
    private String mensagemAviso;

    public void load() {
        Conexao c = new Conexao();
        PreparedStatement ps;
        String SQLSelect = "select * from motorista m"
                + " inner join funcionario f on f.cpf = m.cpf"
                + " inner join pessoa p on p.id = m.cpf"
                + " where cnh = ?";
        
        try{
            ps = c.getConexao().prepareStatement(SQLSelect);
            ps.setLong(1, this.numeroCNH);
            
            ResultSet rs = ps.executeQuery();
            
            if(rs.next()){
                this.setCpf(rs.getLong("id"));
                this.selectAtributos(rs);
                this.selectMaisAtributos(rs);
                this.selectMotoAtributos(rs);
            }
        }
        catch(SQLException e){
           e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }
    
    public static ArrayList<Motorista> getAllAtivos(){
        ArrayList<Motorista> lista = new ArrayList<>();
        Motorista m;
        
        Conexao c = new Conexao();
        Statement s;
        String SQLSelect = "select * from motorista m"
                        + " inner join funcionario f on f.cpf = m.cpf"
                        + " inner join pessoa p on p.id = m.cpf"
                        + " where status = 1";
        
        try{
            s = c.getConexao().createStatement();
            
            ResultSet rs = s.executeQuery(SQLSelect);
            
            while(rs.next()){
                m = new Motorista();
                
                m.setCpf(rs.getLong("id"));
                m.selectAtributos(rs);
                m.selectMaisAtributos(rs);
                m.selectMotoAtributos(rs);
                
                lista.add(m);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
        System.out.println("BBBBBBBBBBBB");
        return lista;
    }

    private void selectMotoAtributos(ResultSet rs) {
        try{
            this.numeroCNH = rs.getLong("cnh");
            this.validadeCNH = rs.getDate("data_cnh");
        }
        catch(SQLException e){
            System.out.println("Erro ao carregar motorista: " + e.getMessage());
        }
    }
}
