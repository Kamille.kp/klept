package Model;

import Usuario.Operador;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.sql.Date;
import lombok.Data;

public @Data class Viagem {
    private int id;
    private ArrayList<Motorista> motoristas = new ArrayList<>();
    private ArrayList<Pessoa> passageiros = new ArrayList<>();
    private Veiculo veiculo;
    private double precoFinal;
    private Pacote pacote;
    private Date horarioChegada;
    private Date horarioPartida;
    private int distancia;
    
    public void setHorarioChegadaByString(String horario){
        String date = horario.replace('/', '-');
        
        this.horarioChegada = Date.valueOf(horario);
    }
    
    public void setHorarioPartidaByString(String horario){
        String date = horario.replace('/', '-');
        
        this.horarioPartida = Date.valueOf(horario);
    }
    
    public String getHorarioChegada(){
        return (String.valueOf(this.horarioChegada).replace('-', '/'));
    }
    
    public String getHorarioPartida(){
        return (String.valueOf(this.horarioPartida).replace('-', '/'));
    }
    
    public void insert(){
        Conexao c = new Conexao();
        PreparedStatement ps;
        String SQLInsert = "insert into viagem"
                                    + " values(seq_viagem,?,?,?,?,?,1,?)";
        
        try{
            ps = c.getConexao().prepareStatement(SQLInsert);
            
            ps.setDate(1, (java.sql.Date) this.horarioChegada);
            ps.setDate(2, (java.sql.Date) this.horarioPartida);
            ps.setDouble(3, this.precoFinal);
            ps.setLong(4, this.veiculo.getRenavam());
            ps.setInt(5, this.pacote.getId());
            ps.setInt(6, this.distancia);
            
            this.insertPassageiros();
            this.insertMotoristas();
            
            ps.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }
    
    public void update(){
        Conexao c = new Conexao();
        PreparedStatement ps;
        String SQLUpdate = "update viagem set"
                                    + "horario_chegada = ?, horario_partida = ?,"
                                    + "preco = ?, cnh_motorista = ?,"
                                    + "renavam_veiculo = ?, id_pacote = ?"
                                    + "where id_viagem = ?";
        
        try{
            ps = c.getConexao().prepareStatement(SQLUpdate);
            
            ps.setInt(7,this.id);
            
            ps.setDate(1, (java.sql.Date) this.horarioChegada);
            ps.setDate(2, (java.sql.Date) this.horarioPartida);
            ps.setDouble(3, this.precoFinal);
            ps.setInt(4,0);
            ps.setLong(5, this.veiculo.getRenavam());
            ps.setInt(6, this.pacote.getId());
            
            ps.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }
    
    public void delete(){
        Conexao c = new Conexao();
        PreparedStatement ps;
        String SQLDelete = "delete from viagem where id = ?";
        
        try{
            ps = c.getConexao().prepareStatement(SQLDelete);
            
            ps.setInt(1,this.id);
            
            ps.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }
    
    public static ArrayList<Viagem> getAll(){
        ArrayList<Viagem> lista = new ArrayList<Viagem>();
        Statement s;
        String SQLSelect = "select * from viagem";
        Conexao c = new Conexao();
        Viagem v;
        
        try{
            s = c.getConexao().createStatement();
            
            ResultSet rs = s.executeQuery(SQLSelect);
            
            while(rs.next()){
                v = new Viagem();
                v.setPacote(new Pacote());
                
                v.setId(rs.getInt("id_viagem"));
                v.setHorarioChegada(rs.getDate("horario_chegada"));
                v.setHorarioPartida(rs.getDate("horario_partida"));
                v.setPrecoFinal(rs.getDouble("preco"));
                v.setVeiculo(Veiculo.getOne(rs.getLong("renavam_veiculo")));
                v.getPacote().setId(rs.getInt("id_pacote"));
                v.getPacote().load();
                
                v.loadPassageiros();
                v.loadMotoristas();
                
                lista.add(v);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
        
        return lista;
    }
    
    public static boolean verificarClienteConexoes(Pessoa p){
        boolean fezViagem = false;
        
        for(Viagem v : Viagem.getAll()){
            if(v.getPassageiros().contains(p)){
                fezViagem = true;
                break;
            }
        }
        
        return fezViagem;
    }
    
    public static boolean verificarPacoteConexoes(Pacote p){
        boolean esta = false;
        
        for(Viagem v : Viagem.getAll()){
            if(v.getPacote().equals(p)){
                esta = true;
                break;
            }
        }
        
        return esta;
    }

    public static boolean verificarVeiculoConexoes(Veiculo v) {
        boolean esta = false;
        
        for(Viagem vi : Viagem.getAll()){
            if(vi.getVeiculo().equals(v)){
                esta = true;
                break;
            }
        }
        
        return esta;
    }

    private void loadPassageiros() {
        PreparedStatement ps;
        Conexao c = new Conexao();
        String SQLSelect = "select * from lotacao where id_viagem = ?";
        Pessoa p;
        
        try{
            ps = c.getConexao().prepareStatement(SQLSelect);
            
            ps.setInt(1, this.id);
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){
                p = new Pessoa();
                p.setCpf(rs.getLong("cpf_pessoa"));
                p.load();
                this.passageiros.add(p);
            }
        }
        catch(SQLException e){
            System.out.println("Erro ao carrgar os passageiros: " + e.getMessage());
        }
        finally{
            c.desconecta();
        }
    }

    private void loadMotoristas() {
        PreparedStatement ps;
        Conexao c = new Conexao();
        String SQLSelect = "select * from motorista_viagem where id_viagem = ?";
        Motorista m;
        
        try{
            ps = c.getConexao().prepareStatement(SQLSelect);
            
            ps.setInt(1, this.id);
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){
                m = new Motorista();
                m.setNumeroCNH(rs.getLong("cnh_motorista"));
                m.load();
                this.motoristas.add(m);
            }
        }
        catch(SQLException e){
            System.out.println("Erro ao carrgar os motoristas: " + e.getMessage());
        }
        finally{
            c.desconecta();
        }
    }
    
    public void addPassageiros(Pessoa p){
        this.passageiros.add(p);
    }
    
    public void addMotoristas(Motorista m){
        this.motoristas.add(m);
    }
    
    public void insertPassageiros(){
        Conexao c = new Conexao();
        PreparedStatement ps;
        String SQLInsert = "insert into lotacao values(seq_lotacao,?,?,1,?,?,?)";
        
        try{
            for(Pessoa p : this.passageiros){
                ps = c.getConexao().prepareStatement(SQLInsert);
                ps.setLong(1, p.getCpf());
                ps.setInt(2, this.id);
                ps.setDate(3, new Date(System.currentTimeMillis()));
                ps.setInt(4, 0);
                ps.setLong(5, Operador.getInfo().getCpf());
                
                ps.executeUpdate();
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }
    
    public void insertMotoristas(){
        Conexao c = new Conexao();
        PreparedStatement ps;
        String SQLInsert = "insert into motorista_viagem values(seq_cobranca.nextval, ?, ?)";
        
        try{
            for(Motorista m : this.motoristas){
                ps = c.getConexao().prepareStatement(SQLInsert);
                ps.setInt(1, this.id);
                ps.setLong(2, m.getNumeroCNH());
                
                ps.executeUpdate();
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            c.desconecta();
        }
    }
    
    @Override
    public String toString(){
        String frase = "Placa do veículo: " + this.veiculo.getPlaca() +
                       "\nId do pacote: " + this.pacote.getId() + 
                       "\n\nMotoristas:";
        
        for(Motorista m : this.motoristas){
            frase += "\n-" + m.getNome();
        }
        
        frase += "\n\nClientes:";
        
        for(Pessoa p : this.passageiros){
            frase += "\n-" + p.getNome();
        }
        
        return frase;
    }
}
