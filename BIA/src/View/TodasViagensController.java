/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Model.Viagem;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class TodasViagensController implements Initializable {

    @FXML private TableView<Viagem> tabViagens;
    @FXML private TableColumn<?, ?> colId;
    @FXML private TableColumn<?, ?> colPreco;
    @FXML private TableColumn<?, ?> colDist;
    @FXML private TableColumn<?, ?> colCheg;
    @FXML private TableColumn<?, ?> colPart;
    @FXML private TableColumn<?, ?> colStatus;
    
    @FXML private Label textinho;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.criarLinhas();
        this.criarColunas();
        
    }
    
    public void criarLinhas(){
        for(Viagem v : Viagem.getAll()){
            this.tabViagens.getItems().add(v);
        }
    }
    
    public void criarColunas(){
        this.colCheg.setCellValueFactory(new PropertyValueFactory<>("horarioChegada"));
        this.colDist.setCellValueFactory(new PropertyValueFactory<>("distancia"));
        this.colId.setCellValueFactory(new PropertyValueFactory<>("id"));
        this.colPart.setCellValueFactory(new PropertyValueFactory<>("horarioPartida"));
        this.colPreco.setCellValueFactory(new PropertyValueFactory<>("preco"));
        this.colStatus.setCellValueFactory(new PropertyValueFactory<>("status"));
    }

    @FXML
    private void alterar(){
        Viagem v = this.tabViagens.getSelectionModel().getSelectedItem();
        
        if(v != null){
            CadastrarViagemController.setV(v);
        
            Main.trocaTela("CadastrarViagem.fxml");
        }
        else{
            this.textinho.setText("Nenhuma viagem selecionada!");
        }
    }

    @FXML
    private void desativar(){
        Viagem v = this.tabViagens.getSelectionModel().getSelectedItem();
        
        if(v != null){
            v.delete();
        }
        else{
            this.textinho.setText("Nenhuma Viagem selecionada!");
        }
    }

    @FXML
    private void cadastrar(){
        Main.trocaTela("CadastrarViagem.fxml");
    }

   @FXML
    private void irCob(){
        Main.trocaTela("TodasCobrancas.fxml");
    }

    @FXML
    private void irPonTur(){
        Main.trocaTela("TodosPontosTuristicos.fxml");
    }

    @FXML
    private void irPaco(){
        Main.trocaTela("TodosPacotes.fxml");
    }

    @FXML
    private void irViag(){
        Main.trocaTela("TodasViagens.fxml");
    }

    @FXML
    private void irVei(){
        Main.trocaTela("TodosVeiculos.fxml");
    }

    @FXML
    private void irFun(){
        Main.trocaTela("TodosFuncionarios.fxml");
    }

    @FXML
    private void irClien(){
        Main.trocaTela("TodosClientes.fxml");
    }
    
    @FXML
    private void logout(){
        Main.trocaTela("Login.fxml");
    }
}
