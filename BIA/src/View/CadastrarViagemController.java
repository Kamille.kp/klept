/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Model.*;
import java.net.URL;
import java.sql.Date;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class CadastrarViagemController implements Initializable {
    @FXML private TableView<Pessoa> tabClientes;
    @FXML private TableColumn<?, ?> colNomeClien;
    @FXML private TableColumn<?, ?> colCpfClien;
    
    @FXML private ChoiceBox<Pacote> opcPac;
    @FXML private ChoiceBox<Veiculo> opcVeic;
    
    @FXML private TableView<Motorista> tabMotor;
    @FXML private TableColumn<?, ?> colNomeMot;
    
    @FXML private Button cadAlt;
    @FXML private TextArea info;
    
    @FXML private Label chegLabel;
    @FXML private TextField partidaTF;
    @FXML private TextField chegadaTF;
    
    private static Viagem v;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.criaColunas();
        this.criaLinhas();
        this.iniciaOpcoes();
        this.trocaAltCad();
    }
    
    public static void setV(Viagem v){
        CadastrarViagemController.v = v;
    }
    
    private void criaLinhas(){
        for(Pessoa p : Pessoa.getClientesAtivos()){
            this.tabClientes.getItems().add(p);
        }
        
        for(Motorista m : Motorista.getAllAtivos()){
            this.tabMotor.getItems().add(m);
        }
    }
    
    private void criaColunas(){
        this.colCpfClien.setCellValueFactory(new PropertyValueFactory<>("id"));
        this.colNomeClien.setCellValueFactory(new PropertyValueFactory<>("nome"));
        this.colNomeMot.setCellValueFactory(new PropertyValueFactory<>("nome"));
    }
    
    private void iniciaOpcoes(){
        for(Pacote p : Pacote.getAllAtivos()){
            this.opcPac.getItems().add(p);
        }
        System.out.println("AAAAAAAAAAAAA");
        for(Veiculo v : Veiculo.getAllAtivos()){
            this.opcVeic.getItems().add(v);
        }
        System.out.println("BBBBBBBBBB");
    }

    private void trocaAltCad(){
        if(v != null){
            this.chegLabel.setVisible(true);
            this.chegadaTF.setVisible(true);
            this.info.setText(v.toString());
            this.cadAlt.setText("Alterar");
        }
        else{
            v = new Viagem();
        }
    }

    @FXML
    private void cadastrarAlterar(){
        boolean nulo;
        
        nulo = v.getPacote() == null;
        nulo = (nulo || v.getVeiculo() == null);
        nulo = (nulo || v.getPassageiros().isEmpty());
        nulo = (nulo || v.getMotoristas().isEmpty());
        
        if(!nulo){
            if(this.cadAlt.getText().equals("Cadastrar")){
                v.setPrecoFinal(v.getPacote().getPreco());
                v.setHorarioPartidaByString(this.partidaTF.getText());
                v.insert();
            }
            else{
                v.setHorarioChegadaByString(this.chegadaTF.getText());
                v.update();
            }
            Main.trocaTela("TodasViagens.fxml");
        }
    }

    @FXML
    private void addCliente(){
        Pessoa p = this.tabClientes.getSelectionModel().getSelectedItem();
        
        if(p != null){
            v.addPassageiros(p);
            this.info.setText(v.toString());
        }
    }

    @FXML
    private void removeCliente(){
        Pessoa p = this.tabClientes.getSelectionModel().getSelectedItem();
        
        if(p != null){
            v.getPassageiros().remove(p);
            this.info.setText(v.toString());
        }
    }

    @FXML
    private void addMotorista(){
        Motorista m = this.tabMotor.getSelectionModel().getSelectedItem();
        
        if(m != null){
            v.addMotoristas(m);
            this.info.setText(v.toString());
        }
    }

    @FXML
    private void removeMotorista(){
        Motorista m = this.tabMotor.getSelectionModel().getSelectedItem();
        
        if(m != null){
            v.getMotoristas().remove(m);
            this.info.setText(v.toString());
        }
    }

    @FXML
    private void selecVeiculo(){
        Veiculo ve = this.opcVeic.getValue();
        
        if(ve != null){
            v.setVeiculo(ve);
            this.info.setText(v.toString());
        }
    }

    @FXML
    private void selecPacote(){
        Pacote p = this.opcPac.getValue();
        
        if(p != null){
            v.setPacote(p);
            this.info.setText(v.toString());
        }
    }

    @FXML
    private void logout(){
        Main.trocaTela("Login.fxml");
    }
    
   @FXML
    private void irCob(){
        Main.trocaTela("TodasCobrancas.fxml");
    }

    @FXML
    private void irPonTur(){
        Main.trocaTela("TodosPontosTuristicos.fxml");
    }

    @FXML
    private void irPaco(){
        Main.trocaTela("TodosPacotes.fxml");
    }

    @FXML
    private void irViag(){
        Main.trocaTela("TodasViagens.fxml");
    }

    @FXML
    private void irVei(){
        Main.trocaTela("TodosVeiculos.fxml");
    }

    @FXML
    private void irFun(){
        Main.trocaTela("TodosFuncionarios.fxml");
    }

    @FXML
    private void irClien(){
        Main.trocaTela("TodosClientes.fxml");
    }
}
