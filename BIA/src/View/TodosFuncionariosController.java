/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Excecoes.ClienteJaTemViagens;
import Model.Funcionario;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class TodosFuncionariosController implements Initializable {

    @FXML private TableView<Funcionario> tabFuncionario;
    @FXML private TableColumn<?, ?> colNome;
    @FXML private TableColumn<?, ?> colCpf;
    @FXML private TableColumn<?, ?> colStatus;
    @FXML private TableColumn<?, ?> colLogin;
    
    @FXML private Label textinho;
    
    private static Funcionario f;
    @FXML
    private Menu menuFun;

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.criarLinhas();
        this.criarColunas();
        
        if(ErroConfirmacaoController.isConfirmacao()){
            f.tornaInativo();
            tabFuncionario.refresh();
            ErroConfirmacaoController.setConfirmacao(false);
        }
    }

    private void criarLinhas() {
        for(Funcionario f : Funcionario.getAll()){
            this.tabFuncionario.getItems().add(f);
        }
    }

    private void criarColunas() {
        colNome.setCellValueFactory(new PropertyValueFactory<>("nome"));
        colCpf.setCellValueFactory(new PropertyValueFactory<>("cpf"));
        colStatus.setCellValueFactory(new PropertyValueFactory<>("status"));
        colLogin.setCellValueFactory(new PropertyValueFactory<>("login"));
    }  

    @FXML
    private void alterar(){
        f = tabFuncionario.getSelectionModel().getSelectedItem();
        
        if(f!=null){
            CadastrarFuncionarioController.setF(f);
            Main.trocaTela("CadastrarFuncionario.fxml");
        }
        else{
            this.textinho.setText("Nenhum cliente selecionado!");
        }
    }

    @FXML
    private void desativar(){
        f = tabFuncionario.getSelectionModel().getSelectedItem();
        
        if(f!=null){
            try{
                f.delete();
                tabFuncionario.getItems().remove(f);
            }
            catch(ClienteJaTemViagens e){
                ErroConfirmacaoController.setErro(e.getMessage());
                ErroConfirmacaoController.setTela("TodosFuncionarios.fxml");
            }
        }
        else{
            this.textinho.setText("Nenhum cliente selecionado!");
        }
    }

    @FXML
    private void cadastrar(){
        Main.trocaTela("CadastrarFuncionario.fxml");
    }

   @FXML
    private void irCob(){
        Main.trocaTela("TodasCobrancas.fxml");
    }

    @FXML
    private void irPonTur(){
        Main.trocaTela("TodosPontosTuristicos.fxml");
    }

    @FXML
    private void irPaco(){
        Main.trocaTela("TodosPacotes.fxml");
    }

    @FXML
    private void irViag(){
        Main.trocaTela("TodasViagens.fxml");
    }

    @FXML
    private void irVei(){
        Main.trocaTela("TodosVeiculos.fxml");
    }

    @FXML
    private void irFun(){
        Main.trocaTela("TodosFuncionarios.fxml");
    }

    @FXML
    private void irClien(){
        Main.trocaTela("TodosClientes.fxml");
    }
    
    @FXML
    private void logout(){
        Main.trocaTela("Login.fxml");
    }
}
