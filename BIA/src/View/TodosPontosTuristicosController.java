/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Excecoes.PontoTuristicoJaTemPacotes;
import Model.PontoTuristico;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author Cliente
 */
public class TodosPontosTuristicosController implements Initializable {
    @FXML private TableView<PontoTuristico> tabPontTur;
    @FXML private TableColumn<?, ?> colNome;
    @FXML private TableColumn<?, ?> colStatus;
    
    @FXML private Label textinho;
    
    private static PontoTuristico pt;

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.criarLinhas();
        this.criarColunas();
        
        if(ErroConfirmacaoController.isConfirmacao()){
            pt.tornaInativo();
            tabPontTur.refresh();
            ErroConfirmacaoController.setConfirmacao(false);
        }
    }

    private void criarLinhas() {
        for(PontoTuristico pt : PontoTuristico.getAll()){
            tabPontTur.getItems().add(pt);
        }
    }

    private void criarColunas() {
        this.colNome.setCellValueFactory(new PropertyValueFactory<>("nome"));
        this.colStatus.setCellValueFactory(new PropertyValueFactory<>("status"));
    }

     @FXML
    private void alterar() {
        pt = tabPontTur.getSelectionModel().getSelectedItem();
        
        if(pt!=null){
            CadastrarPontoTuristicoController.setPt(pt);
            Main.trocaTela("CadastrarPontoTuristico.fxml");
        }
        else{
            this.textinho.setText("Nenhum cliente selecionado!");
        }
    }

    @FXML
    private void desativar(){
        pt = tabPontTur.getSelectionModel().getSelectedItem();
        
        if(pt!=null){
            try{
                pt.delete();
                tabPontTur.getItems().remove(pt);
            }
            catch(PontoTuristicoJaTemPacotes e){
                ErroConfirmacaoController.setErro(e.getMessage());
                ErroConfirmacaoController.setTela("TodosPacotes.fxml");
                Main.trocaTela("ErroConfirmacao.fxml");
            }
        }
        else{
            this.textinho.setText("Nenhum cliente selecionado!");
        }
    }

    @FXML
    private void cadastrar() {
        Main.trocaTela("CadastrarTodos.fxml");
    }

    @FXML
    private void logout(){
        Main.trocaTela("Login.fxml");        
    }

    @FXML
    private void irCob(){
    }

    @FXML
    private void irPonTur(){
        Main.trocaTela("TodosPontosTuristicos.fxml");
    }

    @FXML
    private void irClien(){
        Main.trocaTela("TodosPessoas.fxml");
    }

    @FXML
    private void irViag(){
        Main.trocaTela("TodasViagens.fxml");
    }

    @FXML
    private void irVei(){
        Main.trocaTela("TodosVeiculos.fxml");
    }

    @FXML
    private void irFun(){
        Main.trocaTela("TodosFuncionarios.fxml");
    }
}
