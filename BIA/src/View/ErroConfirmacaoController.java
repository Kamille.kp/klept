/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author Cliente
 */
public class ErroConfirmacaoController implements Initializable{

    @FXML
    private Label erroMensagem;
    
    private static String tela;
    private static String erro;
    private static boolean confirmacao;

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb){
        this.erroMensagem.setText(erro);
    }    

    @FXML
    private void cancelar(){
        confirmacao = false;
        Main.trocaTela(tela);
    }

    @FXML
    private void continuar(){
        confirmacao = true;
        Main.trocaTela(tela);
    }
    
    public static void setTela(String tela){
        ErroConfirmacaoController.tela = tela;
        Main.trocaTela("ErroConfirmacao.fxml");
    }
    
    public static void setErro(String erro){
        ErroConfirmacaoController.erro = erro;
    }

    public static boolean isConfirmacao(){
        return confirmacao;
    }

    public static void setConfirmacao(boolean confirmacao){
        ErroConfirmacaoController.confirmacao = confirmacao;
    }
}