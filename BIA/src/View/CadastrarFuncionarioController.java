/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import APIs.JFrmBuscaCep;
import Excecoes.DataInvalida;
import Excecoes.SenhasDiferentes;
import Model.Endereco;
import Model.Funcionario;
import View.Main;
import java.net.URL;
import java.sql.Date;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;

/**
 * FXML Controller class
 *
 * @author Cliente
 */
public class CadastrarFuncionarioController implements Initializable {
    //Informam os dados pessoais do funcionario
    @FXML private TextField nomeTF;
    @FXML private TextField cpfTF;
    @FXML private TextField nascTF;
    @FXML private TextField teleTF;
    
    //Informam os dados profissionais do funcionario
    @FXML private TextField salarTF;
    @FXML private TextField senhaTF;
    @FXML private TextField repSenhaTF;

    //Informam o endereço do funcionario
    @FXML private TextField cepTF;
    @FXML private TextField logTF;
    @FXML private TextField numTF;
    @FXML private TextField compTF;
    @FXML private TextField bairroTF;
    @FXML private TextField cidadeTF;
    @FXML private TextField estadoTF;
    
    //Botão de cadastrar ou alterar
    @FXML private Button cadAlt;
    
    //Informam o status do funcionario
    @FXML private RadioButton ativoRB;
    @FXML private RadioButton inativoRB;
    
    //Mensagem de erros
    @FXML private Label erros;
    
    //Indica uma pessoa para ser alterada
    private static Funcionario f;
    @FXML
    private ToggleGroup Atividade;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        if(f!=null){
            cadAlt.setText("Alterar");
            this.iniciaEspacos();
            ativoRB.setVisible(true);
            inativoRB.setVisible(true);
        }
    }    

    @FXML
    private void cadastrarAlterar() {
        Funcionario nf = new Funcionario();
        Endereco e = new Endereco();
        
        try{
            nf.setCpf(Integer.parseInt(this.cpfTF.getText()));
            nf.setDataCadastro(new Date(System.currentTimeMillis()));
            f.setDataNascimento(this.nascTF.getText());
            nf.setNome(this.nomeTF.getText());
            nf.setTelefone(Integer.parseInt(this.teleTF.getText()));
        
            nf.setSalario(Double.parseDouble(this.salarTF.getText()));
            nf.setSenhas(this.senhaTF.getText(), this.repSenhaTF.getText());
       
            e.setBairro(this.bairroTF.getText());
            e.setCep(Integer.parseInt(this.cepTF.getText()));
            e.setCidade(this.cidadeTF.getText());
            e.setComplemento(this.compTF.getText());
            e.setEstado(this.estadoTF.getText());
            e.setLogradouro(this.logTF.getText());
            e.setNumero(Integer.parseInt(this.numTF.getText()));
        
            nf.setEndereco(e);
        
            if(CadastrarFuncionarioController.f == null){
                nf.insert();
            }
            else {
                nf.setStatus(ativoRB.isSelected());
                nf.update();
            }
        }
        catch(DataInvalida | SenhasDiferentes ex){
            this.erros.setText(ex.getMessage());
            return;
        }
        
        this.esvaziaCampos();
        this.voltar();
    }
    
    @FXML
    private void buscaCep()
    {
        JFrmBuscaCep a = new JFrmBuscaCep();
         logTF.setText("Aguarde...");
        if (cepTF.getText().length() == 8)
        {
                a.buscarCep(cepTF.getText());
        }
        logTF.setText(a.logradouro);
            bairroTF.setText(a.bairro);
            cidadeTF.setText(a.cidade);
            estadoTF.setText(a.uf);
    }
    
    private void voltar() {
        CadastrarFuncionarioController.f = null;
        Main.trocaTela("TodosClientes.fxml");
    }
    
    public static void setF(Funcionario f){
        CadastrarFuncionarioController.f = f;
    }

    private void iniciaEspacos() {
        Funcionario fu = CadastrarFuncionarioController.f;
        Endereco e = CadastrarFuncionarioController.f.getEndereco();
        
        this.cpfTF.setText(String.valueOf(fu.getCpf()));
        this.nascTF.setText(String.valueOf(fu.getDataNascimento()));
        this.nomeTF.setText(fu.getNome());
        this.teleTF.setText(String.valueOf(fu.getTelefone()));
        
        this.senhaTF.setText(fu.getSenha());
        this.repSenhaTF.setText(fu.getSenha());
        this.salarTF.setText(String.valueOf(fu.getSalario()));
        
        this.bairroTF.setText(e.getBairro());
        this.cepTF.setText(String.valueOf(e.getCep()));
        this.cidadeTF.setText(e.getCidade());
        this.compTF.setText(e.getComplemento());
        this.estadoTF.setText(e.getEstado());
        this.logTF.setText(e.getLogradouro());
        this.numTF.setText(String.valueOf(e.getNumero()));
        
        if(fu.getStatus().equals("Ativo")){
            ativoRB.setSelected(true);
        }
        else{
            inativoRB.setSelected(true);
        }
    }

    private void esvaziaCampos() {
        this.bairroTF.setText("");
        this.cepTF.setText("");
        this.cidadeTF.setText("");
        this.compTF.setText("");
        this.cpfTF.setText("");
        this.estadoTF.setText("");
        this.logTF.setText("");
        this.nascTF.setText("");
        this.nomeTF.setText("");
        this.numTF.setText("");
        this.teleTF.setText("");
    }
    
    @FXML
    private void logout(){
        Main.trocaTela("Login.fxml");        
    }

    @FXML
    private void irPonTur(){
        Main.trocaTela("TodosPontosTuristicos.fxml");
    }

    @FXML
    private void irClien(){
        Main.trocaTela("TodosPessoas.fxml");
    }

    @FXML
    private void irViag(){
        Main.trocaTela("TodasViagens.fxml");
    }

    private void irVei(){
        Main.trocaTela("TodosVeiculos.fxml");
    }

    private void irFun(){
        Main.trocaTela("TodosFuncionarios.fxml");
    }

    @FXML
    private void irPac() {
        Main.trocaTela("TodosPacotes.fxml");
    }
}
