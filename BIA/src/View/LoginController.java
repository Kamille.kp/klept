/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Excecoes.UsuarioInvalido;
import Usuario.Operador;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class LoginController implements Initializable {

    @FXML
    private TextField loginTF;
    @FXML
    private PasswordField senhaPF;

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       // DonoDaEmpresa.setDono();
       Operador.setLogin(null);
       Operador.setSenha(null);
       Operador.setInfo(null);
    }    

    @FXML
    private void entrar() {
        if(this.Autenticacao()){
            Main.trocaTela("Menu.fxml");
        }
    }
    
    
    private boolean Autenticacao(){
        try{
            Operador.setLogin(loginTF.getText());
            Operador.setSenha(senhaPF.getText());
            Operador.testaLogin();
        }
        catch(UsuarioInvalido e){
            return false;
        }
        return true;
    }
}
