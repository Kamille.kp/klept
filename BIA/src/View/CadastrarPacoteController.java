/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Model.Pacote;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;

/**
 * FXML Controller class
 *
 * @author Cliente
 */
public class CadastrarPacoteController implements Initializable {

    @FXML
    private TextField nomeTF;
    @FXML
    private TextField teleTF;
    @FXML
    private Button cadAlt;
    @FXML
    private RadioButton ativoRB;
    @FXML
    private RadioButton inativoRB;
    @FXML
    private TextField teleTF1;
    
    private static Pacote p;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void cadastrarAlterar(ActionEvent event) {
    }
    
    public static void setP(Pacote p){
        CadastrarPacoteController.p = p;
    }
    
    @FXML
    private void logout(){
        Main.trocaTela("Login.fxml");        
    }

    @FXML
    private void irPonTur(){
        Main.trocaTela("TodosPontosTuristicos.fxml");
    }

    @FXML
    private void irClien(){
        Main.trocaTela("TodosPessoas.fxml");
    }

    @FXML
    private void irViag(){
        Main.trocaTela("TodasViagens.fxml");
    }

    private void irVei(){
        Main.trocaTela("TodosVeiculos.fxml");
    }

    private void irFun(){
        Main.trocaTela("TodosFuncionarios.fxml");
    }

    @FXML
    private void irPac() {
        Main.trocaTela("TodosPacotes.fxml");
    }
    
    @FXML
    private void irCob(){
        Main.trocaTela("TodasCobrancas.fxml");
    }
}
