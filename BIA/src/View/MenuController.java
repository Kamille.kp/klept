/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class MenuController implements Initializable {

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void cliente(){
        Main.trocaTela("TodosClientes.fxml");
    }

    @FXML
    private void viagem(){
        Main.trocaTela("TodasViagens.fxml");
    }

    @FXML
    private void pontoTur(){
        Main.trocaTela("TodospontosTuristicos.fxml");
    }

    @FXML
    private void funcio(){
        Main.trocaTela("TodosFuncionarios.fxml");
    }

    @FXML
    private void motorista(){
        
    }

    @FXML
    private void veiculo(){
        Main.trocaTela("TodosVeiculos.fxml");
    }
    
    
    @FXML
    private void logout(){
        Main.trocaTela("Login.fxml");
    }
}
