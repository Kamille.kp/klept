/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import APIs.JFrmBuscaCep;
import Excecoes.DataInvalida;
import Model.*;
import View.Main;
import java.net.URL;
import java.sql.Date;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;

public class CadastrarPessoaController implements Initializable {
    //Informam os dados pessoais do cliente
    @FXML private TextField nomeTF;
    @FXML private TextField cpfTF;
    @FXML private TextField nascTF;
    
    //informam o endereço do cliente
    @FXML private TextField teleTF;
    @FXML private TextField logTF;
    @FXML private TextField numTF;
    @FXML private TextField compTF;
    @FXML private TextField cepTF;
    @FXML private TextField bairroTF;
    @FXML private TextField cidadeTF;
    @FXML private TextField estadoTF;
    
    //Botão de alterar ou cadastrar
    @FXML private Button cadAlt;
    
    //Informam o estado do cliente
    @FXML private RadioButton ativoRB;
    @FXML private RadioButton inativoRB;
    
    //Não implementado
    @FXML private DatePicker cadCal;
    
    //Mensagem de erro
    @FXML private Label erros;
    
    //Indica uma pessoa para ser alterada
    private static Pessoa p;
    
    @Override
    public void initialize(URL url, ResourceBundle rb){
        if(p!=null){
            cadAlt.setText("Alterar");
            this.iniciaEspacos();
            ativoRB.setVisible(true);
            inativoRB.setVisible(true);
        }
    }    

    @FXML
    private void cadastrarAlterar(){
        Pessoa np = new Pessoa();
        Endereco e = new Endereco();
        
        try{
            np.setCpf(Long.parseLong(this.cpfTF.getText()));
            np.setDataCadastro(new Date(System.currentTimeMillis()));
            np.setDataNascimento(this.nascTF.getText());
            np.setNome(this.nomeTF.getText());
            np.setTelefone(Integer.parseInt(this.teleTF.getText()));
        
        
            e.setBairro(this.bairroTF.getText());
            e.setCep(Integer.parseInt(this.cepTF.getText()));
            e.setCidade(this.cidadeTF.getText());
            e.setComplemento(this.compTF.getText());
            e.setEstado(this.estadoTF.getText());
            e.setLogradouro(this.logTF.getText());
            e.setNumero(Integer.parseInt(this.numTF.getText()));
        
            np.setEndereco(e);
        
            if(CadastrarPessoaController.p == null){
                np.insert();
            }
            else {
                np.setStatus(ativoRB.isSelected());
                np.update();
            }
        }
        catch(DataInvalida ex){
            this.erros.setText(ex.getMessage());
            return;
        }
        this.esvaziaCampos();
        this.voltar();
    }
    
    @FXML
    private void buscaCep(){
        JFrmBuscaCep a = new JFrmBuscaCep();
         logTF.setText("Aguarde...");
        if (cepTF.getText().length() == 8)
        {
                a.buscarCep(cepTF.getText());
        }
        logTF.setText(a.logradouro);
            bairroTF.setText(a.bairro);
            cidadeTF.setText(a.cidade);
            estadoTF.setText(a.uf);
    }
    
    public static void setP(Pessoa p){
        CadastrarPessoaController.p = p;
    }
    
    private void voltar(){
        CadastrarPessoaController.p = null;
        Main.trocaTela("TodosClientes.fxml");
    }

    private void iniciaEspacos(){
        Pessoa pe = CadastrarPessoaController.p;
        Endereco e = CadastrarPessoaController.p.getEndereco();
        
        this.bairroTF.setText(e.getBairro());
        this.cepTF.setText(String.valueOf(e.getCep()));
        this.cidadeTF.setText(e.getCidade());
        this.compTF.setText(e.getComplemento());
        this.cpfTF.setText(String.valueOf(pe.getCpf()));
        this.estadoTF.setText(e.getEstado());
        this.logTF.setText(e.getLogradouro());
        this.nascTF.setText(String.valueOf(pe.getDataNascimento()));
        this.nomeTF.setText(pe.getNome());
        this.numTF.setText(String.valueOf(e.getNumero()));
        this.teleTF.setText(String.valueOf(pe.getTelefone()));
        
        if(pe.getStatus().equals("Ativo")){
            ativoRB.setSelected(true);
        }
        else{
            inativoRB.setSelected(true);
        }
    }

    private void esvaziaCampos(){
        this.bairroTF.setText("");
        this.cepTF.setText("");
        this.cidadeTF.setText("");
        this.compTF.setText("");
        this.cpfTF.setText("");
        this.estadoTF.setText("");
        this.logTF.setText("");
        this.nascTF.setText("");
        this.nomeTF.setText("");
        this.numTF.setText("");
        this.teleTF.setText("");
    }
    
    @FXML
    private void logout(){
        Main.trocaTela("Login.fxml");        
    }

    @FXML
    private void irPonTur(){
        Main.trocaTela("TodosPontosTuristicos.fxml");
    }

    @FXML
    private void irClien(){
        Main.trocaTela("TodosPessoas.fxml");
    }

    @FXML
    private void irViag(){
        Main.trocaTela("TodasViagens.fxml");
    }

    private void irVei(){
        Main.trocaTela("TodosVeiculos.fxml");
    }

    private void irFun(){
        Main.trocaTela("TodosFuncionarios.fxml");
    }

    @FXML
    private void irPac() {
        Main.trocaTela("TodosPacotes.fxml");
    }
    
    @FXML
    private void irCob(){
        Main.trocaTela("TodasCobrancas.fxml");
    }
}
