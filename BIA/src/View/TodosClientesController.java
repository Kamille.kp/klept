package View;

import Excecoes.ClienteJaTemViagens;
import Model.*;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class TodosClientesController implements Initializable {
    @FXML private TableView<Pessoa> tabCliente;
    @FXML private TableColumn<?, ?> colNome;
    @FXML private TableColumn<?, ?> colCpf;
    @FXML private TableColumn<?, ?> colCad;
    @FXML private TableColumn<?, ?> colStatus;
    
    @FXML private Label textinho;
    
    private static Pessoa p;    

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.criarColunas();
        this.criarLinhas();
        
        if(ErroConfirmacaoController.isConfirmacao()){
            p.tornaInativo();
            tabCliente.refresh();
            ErroConfirmacaoController.setConfirmacao(false);
        }
    }
    
    private void criarLinhas() {
        for(Pessoa p : Pessoa.getClientes()){
            tabCliente.getItems().add(p);
        }
    }

    private void criarColunas() {
        colCad.setCellValueFactory(new PropertyValueFactory<>("dataCadastro"));
        colNome.setCellValueFactory(new PropertyValueFactory<>("nome"));
        colStatus.setCellValueFactory(new PropertyValueFactory<>("status"));
        colCpf.setCellValueFactory(new PropertyValueFactory<>("cpf"));
    }
    
    @FXML
    private void alterar() {
        p = tabCliente.getSelectionModel().getSelectedItem();
        
        if(p!=null){
            CadastrarPessoaController.setP(p);
            Main.trocaTela("CadastrarPessoa.fxml");
        }
        else{
            this.textinho.setText("Nenhum cliente selecionado!");
        }
    }
    
    @FXML
    private void desativar(){
        p = tabCliente.getSelectionModel().getSelectedItem();
        
        if(p!=null){
            try{
                p.delete();
                System.out.println("AAAAAAAAAAAAA");
                tabCliente.getItems().remove(p);
                System.out.println("BBBBBBBBBBBBBBBB");
            }
            catch(ClienteJaTemViagens e){
                ErroConfirmacaoController.setErro(e.getMessage());
                ErroConfirmacaoController.setTela("TodosClientes.fxml");
                
            }
        }
        else{
            this.textinho.setText("Nenhum cliente selecionado!");
        }
    }

    @FXML
    private void cadastrar() {
        Main.trocaTela("CadastrarPessoa.fxml");
    }
    
    @FXML
    private void irCob(){
        Main.trocaTela("TodasCobrancas");
    }

    @FXML
    private void irPonTur(){
        Main.trocaTela("TodosPontosTuristicos.fxml");
    }

    @FXML
    private void irPaco(){
        Main.trocaTela("TodosPacotes.fxml");
    }

    @FXML
    private void irViag(){
        Main.trocaTela("TodasViagens.fxml");
    }

    @FXML
    private void irVei(){
        Main.trocaTela("TodosVeiculos.fxml");
    }

    @FXML
    private void irFun(){
        Main.trocaTela("TodosFuncionarios.fxml");
    }
    
    private void logout(){
        Main.trocaTela("Login.fxml");
    }
}