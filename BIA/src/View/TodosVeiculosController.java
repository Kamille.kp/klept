/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Excecoes.VeiculoJaTemViagens;
import Model.Veiculo;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class TodosVeiculosController implements Initializable {
    @FXML private TableView<Veiculo> tabVeiculos;
    @FXML private TableColumn<?, ?> colPlaca;
    @FXML private TableColumn<?, ?> colQuilo;
    @FXML private TableColumn<?, ?> colCad;
    @FXML private TableColumn<?, ?> colTipo;
    @FXML private TableColumn<?, ?> colStatus;
    
    @FXML private Label textinho;
    
    private static Veiculo v;
    @FXML
    private Menu menuVei;
    @FXML
    private Menu menuFun;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.criarLinhas();
        this.criarColunas();
        
        if(ErroConfirmacaoController.isConfirmacao()){
            v.tornaInativo();
            tabVeiculos.refresh();
            ErroConfirmacaoController.setConfirmacao(false);
        }
    }
    
    private void criarLinhas(){
        for(Veiculo v : Veiculo.getAll()){
            this.tabVeiculos.getItems().add(v);
        }
    }
    
    private void criarColunas(){
        colPlaca.setCellValueFactory(new PropertyValueFactory<>("placa"));
        colQuilo.setCellValueFactory(new PropertyValueFactory<>("quilometragem"));
        colCad.setCellValueFactory(new PropertyValueFactory<>(""));
    }

    @FXML
    private void alterar(){
        v = tabVeiculos.getSelectionModel().getSelectedItem();
        
        if(v!=null){
            CadastrarVeiculoController.setV(v);
            Main.trocaTela("CadastrarVeiculo.fxml");
        }
        else{
            this.textinho.setText("Nenhum cliente selecionado!");
        }
    }

    @FXML
    private void desativar(){
        v = this.tabVeiculos.getSelectionModel().getSelectedItem();
        
       if(v != null){
           try{
               v.delete();
           }
           catch(VeiculoJaTemViagens e){
                ErroConfirmacaoController.setErro(e.getMessage());
                ErroConfirmacaoController.setTela("TodosVeiculos.fxml");
           }
       }
       else{
           this.textinho.setText("Nenhum veículo selecionado!");
       }
    }

    @FXML
    private void cadastrar(){
        Main.trocaTela("CadastrarVeiculo.fxml");
    }

    @FXML
    private void logout(){
        Main.trocaTela("Login.fxml");        
    }

    @FXML
    private void irCob(){
    }

    @FXML
    private void irPonTur(){
        Main.trocaTela("TodosPontosTuristicos.fxml");
    }

    @FXML
    private void irClien(){
        Main.trocaTela("TodosPessoas.fxml");
    }

    @FXML
    private void irViag(){
        Main.trocaTela("TodasViagens.fxml");
    }

    @FXML
    private void irVei(){
        Main.trocaTela("TodosVeiculos.fxml");
    }

    @FXML
    private void irFun(){
        Main.trocaTela("TodosFuncionarios.fxml");
    }

    @FXML
    private void irPaco(ActionEvent event) {
    }

    @FXML
    private void irTipoVei(ActionEvent event) {
    }
}
