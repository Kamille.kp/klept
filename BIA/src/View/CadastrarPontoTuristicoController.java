/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import APIs.JFrmBuscaCep;
import Model.Endereco;
import Model.PontoTuristico;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Cliente
 */
public class CadastrarPontoTuristicoController implements Initializable {
    
    private static PontoTuristico pt;

    public static void setPt(PontoTuristico pt) {
        CadastrarPontoTuristicoController.pt = pt;
    }
    @FXML
    private TextField nomeTF;
    @FXML
    private TextField cidadeTF;
    @FXML
    private TextField estTF;
    @FXML
    private TextField bairroTF;
    @FXML
    private TextField descricaoTA;
    @FXML
    private TextField cepTF;
    @FXML
    private TextField numeroTF;
    @FXML
    private TextField compTF;
    
    JFrmBuscaCep a = new JFrmBuscaCep();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        if(pt != null){
            this.iniciarEspacos();
        }
    }

    @FXML
    private void cadAlt() {
        PontoTuristico npt = new PontoTuristico();
        Endereco e = new Endereco();
        
            npt.setNome(this.nomeTF.getText());
            npt.setDescricao(this.descricaoTA.getText());
        
        
            e.setBairro(this.bairroTF.getText());
            e.setCep(Integer.parseInt(this.cepTF.getText()));
            e.setCidade(this.cidadeTF.getText());
            e.setComplemento(this.compTF.getText());
            e.setEstado(this.estTF.getText());
            e.setLogradouro(a.logradouro);
            e.setNumero(Integer.parseInt(this.numeroTF.getText()));
        
            npt.setEndereco(e);
        
            if(CadastrarPontoTuristicoController.pt == null){
                npt.insert();
            }
            else {
                npt.update();
            }
            
        Main.trocaTela("TodosPontosTuristicos.fxml");
    }
    
    @FXML
    private void buscaCep(){
        JFrmBuscaCep a = new JFrmBuscaCep();
        compTF.setText("Aguarde...");
        if (cepTF.getText().length() == 8)
        {
                a.buscarCep(cepTF.getText());
        }
        bairroTF.setText(a.bairro);
        cidadeTF.setText(a.cidade);
        estTF.setText(a.uf);
    }

    
    private void iniciarEspacos() {
        PontoTuristico pt = CadastrarPontoTuristicoController.pt;
        Endereco e = CadastrarPontoTuristicoController.pt.getEndereco();
        
        this.bairroTF.setText(e.getBairro());
        this.cepTF.setText(String.valueOf(e.getCep()));
        this.cidadeTF.setText(e.getCidade());
        this.compTF.setText(e.getComplemento());
        this.estTF.setText(e.getEstado());
        this.nomeTF.setText(pt.getNome());
        this.numeroTF.setText(String.valueOf(e.getNumero()));
        this.descricaoTA.setText(pt.getDescricao());
    }
    
    private void logout(){
        Main.trocaTela("Login.fxml");        
    }

    @FXML
    private void irPonTur(){
        Main.trocaTela("TodosPontosTuristicos.fxml");
    }

    @FXML
    private void irClien(){
        Main.trocaTela("TodosPessoas.fxml");
    }

    @FXML
    private void irViag(){
        Main.trocaTela("TodasViagens.fxml");
    }

    private void irVei(){
        Main.trocaTela("TodosVeiculos.fxml");
    }

    private void irFun(){
        Main.trocaTela("TodosFuncionarios.fxml");
    }

    @FXML
    private void irPac() {
        Main.trocaTela("TodosPacotes.fxml");
    }
    
    private void irCob(){
        Main.trocaTela("TodasCobrancas.fxml");
    }
}