/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Excecoes.PacoteJaTemViagens;
import Model.Pacote;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author Cliente
 */
public class TodosPacotesController implements Initializable {
    @FXML private TableView<Pacote> tabPacotes;
    @FXML private TableColumn<?, ?> colNome;
    @FXML private TableColumn<?, ?> colId;
    @FXML private TableColumn<?, ?> colPreco;
    @FXML private TableColumn<?, ?> colStatus;
    
    @FXML private TextArea descricao;
    
    @FXML private Text textinho;
    
    private static Pacote p;
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.criarLinhas();
        this.criarColunas();
        
        if(ErroConfirmacaoController.isConfirmacao()){
            p.tornaInativo();
            tabPacotes.refresh();
            ErroConfirmacaoController.setConfirmacao(false);
        }
    }

    private void criarLinhas() {
        for(Pacote p : Pacote.getAll()){
            tabPacotes.getItems().add(p);
        }
    }

    private void criarColunas() {
        this.colId.setCellValueFactory(new PropertyValueFactory<>("id"));
        this.colNome.setCellValueFactory(new PropertyValueFactory<>("nome"));
        this.colPreco.setCellValueFactory(new PropertyValueFactory<>("preco"));
        this.colStatus.setCellValueFactory(new PropertyValueFactory<>("status"));
    }

     @FXML
    private void alterar() {
        p = tabPacotes.getSelectionModel().getSelectedItem();
        
        if(p!=null){
            CadastrarPacoteController.setP(p);
            Main.trocaTela("Cadastrar.fxml");
        }
        else{
            this.textinho.setText("Nenhum cliente selecionado!");
        }
    }

    @FXML
    private void desativar(){
        p = tabPacotes.getSelectionModel().getSelectedItem();
        
        if(p!=null){
            try{
                p.delete();
                tabPacotes.getItems().remove(p);
            }
            catch(PacoteJaTemViagens e){
                ErroConfirmacaoController.setErro(e.getMessage());
                ErroConfirmacaoController.setTela("TodosPacotes.fxml");
                Main.trocaTela("ErroConfirmacao.fxml");
            }
        }
        else{
            this.textinho.setText("Nenhum cliente selecionado!");
        }
    }

    @FXML
    private void cadastrar() {
        Main.trocaTela("CadastrarTodos.fxml");
    }

    @FXML
    private void logout(){
        Main.trocaTela("Login.fxml");        
    }

    @FXML
    private void irCob(){
    }

    @FXML
    private void irPonTur(){
        Main.trocaTela("TodosPontosTuristicos.fxml");
    }

    @FXML
    private void irClien(){
        Main.trocaTela("TodosPessoas.fxml");
    }

    @FXML
    private void irViag(){
        Main.trocaTela("TodasViagens.fxml");
    }

    @FXML
    private void irVei(){
        Main.trocaTela("TodosVeiculos.fxml");
    }

    @FXML
    private void irFun(){
        Main.trocaTela("TodosFuncionarios.fxml");
    }
    
    @FXML
    private void mostraDescricao(){
        p = this.tabPacotes.getSelectionModel().getSelectedItem();
        
        if(p != null){
            this.descricao.setText(p + "");
        }
    }
}