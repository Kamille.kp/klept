/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Model.TipoVeiculo;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class CadastrarTipoVeiculoController implements Initializable {

    @FXML
    private TextField nomeTF;
    @FXML
    private TextField quntTF;
    @FXML
    private RadioButton ativoRB;
    @FXML
    private ToggleGroup Atividade;
    @FXML
    private RadioButton inativoRB;
    @FXML
    private TextField combTF;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void cadastrarAlterar() {
        TipoVeiculo tv = new TipoVeiculo();
        
        tv.setNome(nomeTF.getText());
        tv.setCombustivel(combTF.getText());
        tv.setQuantLugares(Integer.parseInt(quntTF.getText()));
    }
    
    private void logout(){
        Main.trocaTela("Login.fxml");        
    }

    private void irPonTur(){
        Main.trocaTela("TodosPontosTuristicos.fxml");
    }

    private void irClien(){
        Main.trocaTela("TodosPessoas.fxml");
    }

    private void irViag(){
        Main.trocaTela("TodasViagens.fxml");
    }

    private void irVei(){
        Main.trocaTela("TodosVeiculos.fxml");
    }

    private void irFun(){
        Main.trocaTela("TodosFuncionarios.fxml");
    }

    private void irPac() {
        Main.trocaTela("TodosPacotes.fxml");
    }
    
    private void irCob(){
        Main.trocaTela("TodasCobrancas.fxml");
    }
}
