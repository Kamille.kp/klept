/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class TodasCobrancasController implements Initializable {

    @FXML
    private TableView<?> tabCliente;
    @FXML
    private TableColumn<?, ?> colNome;
    @FXML
    private TableColumn<?, ?> colCpf;
    @FXML
    private TableColumn<?, ?> colCad;
    @FXML
    private TableColumn<?, ?> colStatus;
    @FXML
    private Label textinho;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void cadastrar(ActionEvent event) {
    }

    @FXML
    private void cadastrar() {
        Main.trocaTela("CadastrarTodos.fxml");
    }
    
    @FXML
    private void irCob(){
    }

    @FXML
    private void irPonTur(){
        Main.trocaTela("TodosPontosTuristicos.fxml");
    }

    @FXML
    private void irPaco(){
        Main.trocaTela("TodosPacotes.fxml");
    }

    @FXML
    private void irViag(){
        Main.trocaTela("TodasViagens.fxml");
    }

    @FXML
    private void irVei(){
        Main.trocaTela("TodosVeiculos.fxml");
    }

    @FXML
    private void irFun(){
        Main.trocaTela("TodosFuncionarios.fxml");
    }
    
    private void logout(){
        Main.trocaTela("Login.fxml");
    }
}