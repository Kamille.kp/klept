/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Model.TipoVeiculo;
import Model.Veiculo;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Cliente
 */
public class CadastrarVeiculoController implements Initializable {

    static void setV(Veiculo v) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @FXML
    private TextField renavTF;
    @FXML
    private TextField placaTF;
    @FXML
    private TextField chassiTf;
    @FXML
    private TextField anoTF;
    @FXML
    private TextField quiloTF;
    @FXML
    private ChoiceBox<TipoVeiculo> TiposVeiculos;
    @FXML
    private TextArea descTF;
    
    private static Veiculo v;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.criaOpcoes();
    }    

    private void criaOpcoes() {
        for(TipoVeiculo tv : TipoVeiculo.getAllAtivos()){
            this.TiposVeiculos.getItems().add(tv);
        }
    }
    
    @FXML
    private void logout(){
        Main.trocaTela("Login.fxml");        
    }

    @FXML
    private void irPonTur(){
        Main.trocaTela("TodosPontosTuristicos.fxml");
    }

    @FXML
    private void irClien(){
        Main.trocaTela("TodosPessoas.fxml");
    }

    @FXML
    private void irViag(){
        Main.trocaTela("TodasViagens.fxml");
    }

    private void irVei(){
        Main.trocaTela("TodosVeiculos.fxml");
    }

    private void irFun(){
        Main.trocaTela("TodosFuncionarios.fxml");
    }

    @FXML
    private void irPac() {
        Main.trocaTela("TodosPacotes.fxml");
    }
    
    @FXML
    private void irCob(){
        Main.trocaTela("TodasCobrancas.fxml");
    }
}
